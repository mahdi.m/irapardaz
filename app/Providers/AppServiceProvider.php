<?php

namespace App\Providers;

use App\Facades\ResponderProviderFacade;
use App\Services\Responses\ApiResponderProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        ResponderProviderFacade::shouldProxyTo(ApiResponderProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes();

        Passport::tokensCan([
            'student' => 'login token for student',
        ]);
    }
}
