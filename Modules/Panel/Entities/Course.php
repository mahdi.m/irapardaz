<?php

namespace Modules\Panel\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Panel\Database\factories\CourseFactory;

class Course extends \App\Models\Course
{
    use HasFactory;


    protected static function newFactory()
    {
        return CourseFactory::new();
    }
}
