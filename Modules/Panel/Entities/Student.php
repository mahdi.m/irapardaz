<?php

namespace Modules\Panel\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Panel\Database\factories\StudentFactory;

class Student extends \App\Models\Student
{
    use HasFactory;


    protected static function newFactory()
    {
        return StudentFactory::new();
    }
}
