<?php


namespace Modules\Panel\Services\Student;


use Modules\Panel\Entities\Student;

class StudentDestroyService extends StudentCommonService
{
    public function destroy(Student $student)
    {
        return $this->repository->delete($student);
    }
}
