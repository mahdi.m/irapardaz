<?php


namespace Modules\Panel\Services\Student;


use Modules\Panel\Entities\Student;

class StudentDisplayService extends StudentCommonService
{
    public function show(Student $student)
    {
        return $student;
    }
}
