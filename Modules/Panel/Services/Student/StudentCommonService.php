<?php


namespace Modules\Panel\Services\Student;


use App\Repositories\BaseRepository;
use App\Services\BaseService;
use Modules\Panel\Repositories\StudentRepository;

class StudentCommonService extends BaseService
{
    public BaseRepository $repository;

    public function __construct(StudentRepository $repository)
    {
        $this->repository = $repository;
    }
}
