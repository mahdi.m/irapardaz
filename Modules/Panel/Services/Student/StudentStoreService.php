<?php


namespace Modules\Panel\Services\Student;


use Illuminate\Http\Request;

class StudentStoreService extends StudentCommonService
{
    public function store(Request $request)
    {
        return $this->repository->store($request->all());
    }
}
