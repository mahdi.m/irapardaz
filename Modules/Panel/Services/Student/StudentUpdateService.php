<?php


namespace Modules\Panel\Services\Student;


use Illuminate\Http\Request;
use Modules\Panel\Entities\Student;

class StudentUpdateService extends StudentCommonService
{
    public function update(Request $request, Student $student)
    {
        $this->repository->update($request->all(), $student);
        return $student;
    }
}
