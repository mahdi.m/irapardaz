<?php


namespace Modules\Panel\Services\Course;


use App\Facades\ResponderProviderFacade;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Panel\Entities\Course;

class AttachStudentToCourseStoreService extends CourseCommonService
{
    public function attachStudentToCourse(Request $request)
    {
        $course = $this->repository->findModel(Course::class, $request->course_id);
        $course->load('students');
        $studentHasCourse = $this->repository->studentHasCourse($course, $request->course_id);
        if ($studentHasCourse) return ResponderProviderFacade::error(Response::HTTP_FORBIDDEN, __('messages.course-student.remaining-is-full'));

        return $this->repository->attachStudentToCourse($course, $request->student_id);
    }
}
