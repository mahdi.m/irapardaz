<?php


namespace Modules\Panel\Services\Course;


class CoursePaginateService extends CourseCommonService
{
    public function paginate(int $perPage, array $relations = [])
    {
        return $this->repository->paginate($perPage, $relations);
    }

    public function index(array $relations = [])
    {
        return $this->repository->all($relations);
    }
}
