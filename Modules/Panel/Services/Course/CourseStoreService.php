<?php


namespace Modules\Panel\Services\Course;


use Illuminate\Http\Request;

class CourseStoreService extends CourseCommonService
{
    public function store(Request $request)
    {
        $remaining = $request->capacity;
        $data = array_merge($request->all(), compact('remaining'));
        return $this->repository->store($data);
    }
}
