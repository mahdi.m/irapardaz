<?php


namespace Modules\Panel\Services\Course;


use App\Repositories\BaseRepository;
use App\Services\BaseService;
use Modules\Panel\Repositories\CourseRepository;

class CourseCommonService extends BaseService
{
    public BaseRepository $repository;

    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }
}
