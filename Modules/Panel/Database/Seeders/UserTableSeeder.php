<?php

namespace Modules\Panel\Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $user = User::query()->where('email', 'admin@afmin.com')->first();
        if (!$user) {
            User::query()->create([
                'name' => 'admin',
                'email' => 'admin@afmin.com',
                'password' => Hash::make('123456')
            ]);
        }

        // $this->call("OthersTableSeeder");
    }
}
