<?php

namespace Modules\Panel\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Panel\Entities\Course::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $capacity = $this->faker->numberBetween(10, 30);
        return [
            'title' => $this->faker->title,
            'number' => $this->faker->numberBetween(1000,2000),
//            'number' => $this->faker->unique(true),
            'capacity' => $capacity,
            'remaining' => $capacity,
        ];
    }
}

