<?php

namespace Modules\Panel\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function findUserByEmail($email)
    {
        return $this->model()::where('email', $email)->first();
    }

}
