<?php

namespace Modules\Panel\Repositories;

use App\Facades\ResponderProviderFacade;
use App\Repositories\BaseRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Panel\Entities\Course;
use Modules\Panel\Entities\Student;

class CourseRepository extends BaseRepository
{
    public function model()
    {
        return Course::class;
    }

    public function studentHasCourse(Course $course, $studentId): bool
    {
        return $course->students()->where('id', $studentId)->exists();
    }

    public function attachStudentToCourse(Course $course, $studentId)
    {
        try {
            DB::beginTransaction();
            $course->decrement('remaining');
            $course->students()->sync([$studentId]);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            return ResponderProviderFacade::error(Response::HTTP_FORBIDDEN, __('messages.course-student.remaining-is-full'));
        }

        return $course->load('students');
    }
}
