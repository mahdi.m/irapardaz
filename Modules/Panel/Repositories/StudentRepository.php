<?php

namespace Modules\Panel\Repositories;

use App\Repositories\BaseRepository;
use Modules\Panel\Entities\Student;

class StudentRepository extends BaseRepository
{
    public function model()
    {
        return Student::class;
    }

    public function paginate(int $perPage, array $relations = [])
    {
        return Student::query()->with(['courses' => function ($q) {
            $q->select('id', 'title');
        }])->paginate($perPage);
    }

}
