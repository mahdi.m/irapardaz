<?php

namespace Modules\Panel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Panel\Http\Requests\AttachStudentToCourseRequest;
use Modules\Panel\Http\Requests\CourseRequest;
use Modules\Panel\Services\Course\AttachStudentToCourseStoreService;
use Modules\Panel\Services\Course\CoursePaginateService;
use Modules\Panel\Services\Course\CourseStoreService;
use Modules\Panel\Transformers\CourseResource;

/**
 * @group Panel
 * @authenticated
 */
class CourseController extends Controller
{
    /**
     * Course display all.
     * @param CoursePaginateService $faqPaginateService
     * @return mixed
     */
    public function index(CoursePaginateService $faqPaginateService)
    {
        return $faqPaginateService->index();
    }

    /**
     * Course paginate.
     *
     * @urlParam perPage int count in per pge
     * @param CoursePaginateService $faqPaginateService
     * @param null $perPage
     * @return mixed|CourseResource
     */
    public function paginate(CoursePaginateService $faqPaginateService, $perPage = null)
    {
        if (!$perPage) return $this->index($faqPaginateService);
        return CourseResource::collection($faqPaginateService->paginate((int)$perPage));
    }

    /**
     * Course store.
     *
     * ایجاد درس
     *
     * @bodyParam title string required عنوان درس
     * @bodyParam number string required شماره درس
     * @bodyParam capacity integer required ظرفیت
     * @param CourseRequest $request
     * @param CourseStoreService $faqStoreService
     * @return CourseResource
     */
    public function store(CourseRequest $request, CourseStoreService $faqStoreService)
    {
        return $faqStoreService->store($request);
    }

    /**
     * Attach Student to Course.
     * اضافه کردن دانش آموز به کلاس
     * @param AttachStudentToCourseRequest $request
     * @param AttachStudentToCourseStoreService $attachStudentToCourseStoreService
     * @return \Modules\Panel\Entities\Course
     */
    public function attachStudentToCourse(AttachStudentToCourseRequest $request, AttachStudentToCourseStoreService $attachStudentToCourseStoreService)
    {
        return $attachStudentToCourseStoreService->attachStudentToCourse($request);
    }
}
