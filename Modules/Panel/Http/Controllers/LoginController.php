<?php

namespace Modules\Panel\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Panel\Facades\UserProviderFacade;
use Modules\Panel\Http\Requests\LoginRequest;

/**
 * Class LoginController
 * @package Modules\Panel\Http\Controllers
 * @group Panel
 */
class LoginController extends Controller
{
    /**
     * Panel Login
     * @bodyParam email string required
     * @bodyParam password string required
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $user = UserProviderFacade::findUserByEmail($request->email);

        $passwordVerify = UserProviderFacade::passwordVerify($request->password, $user->password);
        if ($passwordVerify instanceof JsonResponse) return $passwordVerify;


        $userWithToken = UserProviderFacade::createUserToken($user);
        return $userWithToken;
    }
}
