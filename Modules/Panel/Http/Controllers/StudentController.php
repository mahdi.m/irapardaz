<?php

namespace Modules\Panel\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Panel\Http\Requests\StudentRequest;
use Modules\Panel\Services\Student\StudentPaginateService;
use Modules\Panel\Services\Student\StudentStoreService;
use Modules\Panel\Transformers\StudentCollection;
use Modules\Panel\Transformers\StudentResource;

/**
 * @group Panel
 * @authenticated
 */
class StudentController extends Controller
{
    /**
     * Student display all.
     * @param StudentPaginateService $faqPaginateService
     * @return mixed
     */
    public function index(StudentPaginateService $faqPaginateService)
    {
        return $faqPaginateService->index();
    }

    /**
     * Student paginate.
     *
     * @urlParam perPage int count in per pge
     * @param StudentPaginateService $faqPaginateService
     * @param null $perPage
     * @return mixed|StudentCollection
     */
    public function paginate(StudentPaginateService $faqPaginateService, $perPage = null)
    {
        if (!$perPage) return $this->index($faqPaginateService);
        return StudentResource::collection($faqPaginateService->paginate((int)$perPage));
    }

    /**
     * Student store.
     *
     * @bodyParam name string required نام دانشجو
     * @bodyParam number string required شماره دانشجو
     * @param StudentRequest $request
     * @param StudentStoreService $faqStoreService
     * @return mixed
     */
    public function store(StudentRequest $request, StudentStoreService $faqStoreService)
    {
        return $faqStoreService->store($request);
    }

}
