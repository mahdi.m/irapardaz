<?php

namespace Modules\Panel\Tests\Feature\Controllers\Resource;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Testing\Fluent\AssertableJson;
use Modules\Panel\Entities\Course;
use Modules\Panel\Entities\Student;
use Tests\TestCase;

class CourseControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testPaginate()
    {
        $this->passportLogin();
        $response = $this->json('GET', route('courses.paginate', ['perPage' => 5]));
        $this->assertInstanceOf(Collection::class, $response->getOriginalContent());
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'number',
                    'capacity',
                    'remaining',
                ],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);

        $response->assertOk();
    }


    public function testStore()
    {
        $this->passportLogin();

        $data = [
            'title' => 'test course title',
            'number' => 123123,
            'capacity' => 30,
        ];
        $response = $this->json(
            'POST',
            route('courses.store'),
            $data
        );
        $data = array_merge($data, ['remaining' => 30]);
        $response
            ->assertCreated()
            ->assertJson($data);
        $this->assertDatabaseHas(Course::class, $data);
    }

    public function testAttachStudentToCourseIsFull()
    {
        $this->withoutExceptionHandling();
        $this->passportLogin();
        $course = Course::factory()->create(['remaining' => 0]);
        $student = Student::factory()->create();

        $data = [
            'course_id' => $course->id,
            'student_id' => $student->id,
        ];

        $response = $this->json(
            'POST',
            route('attach.student.to.course'),
            $data
        );

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertJson(['message' => __('messages.course-student.remaining-is-full')]);
    }

    public function testAttachStudentToCourse()
    {
        $this->withoutExceptionHandling();
        $this->passportLogin();
        $course = Course::factory()->create(['remaining' => 2]);
        $student = Student::factory()->create();

        $data = [
            'course_id' => $course->id,
            'student_id' => $student->id,
        ];

        $response = $this->json(
            'POST',
            route('attach.student.to.course'),
            $data
        );

        $response->assertOk();
        $this->assertDatabaseHas('course_student', $data);
    }


}
