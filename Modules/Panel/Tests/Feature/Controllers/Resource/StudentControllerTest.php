<?php

namespace Modules\Panel\Tests\Feature\Controllers\Resource;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Illuminate\Testing\Fluent\AssertableJson;
use Modules\Panel\Entities\Course;
use Modules\Panel\Entities\Student;
use Tests\TestCase;

class StudentControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testPaginate()
    {
        $this->passportLogin();
        Student::factory()->has(Course::factory())->create();
        $response = $this->json('GET', route('students.paginate', ['perPage' => 5]));
        $this->assertInstanceOf(Collection::class, $response->getOriginalContent());
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'number',
                    'courses' => [
                        '*' => [
                            'id',
                            'title',
                        ],
                    ],
                ],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
        $response->assertOk();
    }


    public function testStore()
    {
        $this->passportLogin();

        $data = [
            'name' => 'test student name',
            'number' => 123123,
        ];
        $response = $this->json(
            'POST',
            route('students.store'),
            $data
        );
        $response
            ->assertCreated()
            ->assertJson($data);
        $this->assertDatabaseHas(Student::class, $data);
    }

}
