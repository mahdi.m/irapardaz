<?php

namespace Modules\Panel\Tests\Feature;

use App\Models\User;
use Modules\Panel\Facades\UserProviderFacade;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * test panel login
     */
    public function testLogin()
    {
        User::unguard();

        $request = [
            "email" => "ssmns@gmail.com",
            "password" => '123456'
        ];
        $user = new User(['id' => 1, 'email' => 'admin@admin.com', 'name' => 'admin', 'password' => '123456']);
        $userWithToken = new User(['id' => 1, 'email' => 'admin@admin.com', 'api_token' => 'sdfsdfdsf']);

        UserProviderFacade::shouldReceive('findUserByEmail')
            ->once()
            ->with($request['email'])
            ->andReturn($user);

        UserProviderFacade::shouldReceive('passwordVerify')
            ->once()
            ->with($request['password'], $user->password)
            ->andReturn($user);

        $t = UserProviderFacade::shouldReceive('createUserToken')
            ->once()
            ->with($user)
            ->andReturn($userWithToken);
        $res = $this->json('POST', route('panel.login'), ['email' => $request['email'], 'password' => $request['password']]);
        $res->assertStatus(200);
    }
}
