<?php

use Illuminate\Support\Facades\Route;
use Modules\Panel\Http\Controllers\CourseController;
use Modules\Panel\Http\Controllers\StudentController;
use Modules\Panel\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('panel')->group(function () {

    Route::post('login', [LoginController::class, 'login'])->name('panel.login');

    Route::middleware('auth:api')->group(function () {
        Route::get('students/paginate/{perPage?}', [StudentController::class, 'paginate'])->name('students.paginate');
        Route::resource('students', StudentController::class);

        Route::get('courses/paginate/{perPage?}', [CourseController::class, 'paginate'])->name('courses.paginate');
        Route::resource('courses', CourseController::class);
        Route::post('attach-student-to-course', [CourseController::class, 'attachStudentToCourse'])->name('attach.student.to.course');

    });
});
